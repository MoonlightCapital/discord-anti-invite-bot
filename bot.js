const http = require('http');
const express = require('express');
const app = express();
app.get("/", (request, response) => {
  response.sendStatus(200);
});
app.listen(process.env.PORT);
setInterval(() => {
  http.get(`http://${process.env.PROJECT_DOMAIN}.glitch.me/`);
}, 280000);

let clientOptions = {
  disableEveryone: true,
  disabledEvents: [
    'TYPING_START', 'CHANNEL_CREATE', 'CHANNEL_DELETE', 'CHANNEL_UPDATE', 'GUILD_ROLE_CREATE', 'GUILD_ROLE_DELETE', 'GUILD_ROLE_UPDATE', 'RELATIONSHIP_ADD',
    'RELATIONSHIP_REMOVE', 'VOICE_SERVER_UPDATE', 'GUILD_BAN_REMOVE', 'GUILD_BAN_ADD'
  ]
};

const Discord = require('discord.js');
const {config} = require('./config.js');
const client = new Discord.Client();
const ms = require('ms');

const prefix = '+';

//client.db = require('quick.db');

client.on('ready', () => {
  console.log(`I'm ready!`);

  client.user.setPresence({ game: { name: `spammers getting banned, ${prefix}stats`, type: 'WATCHING' }});
})

client.on('guildMemberAdd', member => {

  let pattern = new RegExp('discord(\\.gg|app\\.com/invite|\\.io)/\\w+', 'gmi');

  if(pattern.test(member.user.username)) {
    
    member.guild.ban(member.id, {reason: 'Invite link as username', days: 7}).catch(console.error);
    client.guilds.filter(g=>g.me.hasPermission('BAN_MEMBERS')).forEach(guild => {
      guild.ban(member.id, 'Invite link as username').catch(console.error);
      //client.db.add('bans', 1);
    });
  }
});

client.on('message', message => {
  if(message.author.bot) return;
  if(message.content.startsWith(prefix+'stats')) {
    message.channel.send(`**\`===Bot statistics===\`**

**Uptime:** ${ms(client.uptime, {long: true})}
**Server count:** ${client.guilds.size}
**Total spammers banned:** (removed due to issues)

*For bot support, join <https://discord.gg/hNQWVVC>*
Invite this bot to your server: <https://discordapp.com/api/oauth2/authorize?client_id=493299837358440468&permissions=3076&scope=bot>
`).catch(console.error);
  }
  
  else if(message.content.startsWith(prefix+'gban')) {
    if(message.author.id !== config.owner) return;
    client.guilds.filter(g=>g.me.hasPermission('BAN_MEMBERS')).forEach(guild => {
      guild.ban(message.content.substring(5), 'Manual ban: account created to scrape invites') //ok, this is the worst way to declare arguments ever.
    });
    
    message.react('466238645095890945');
  }
  
  else if(message.content.startsWith(prefix+'help')) {
    message.channel.send(`
**\`===Bot help===\`**
There's no need to setup the bot. You must give it Ban Members permission if you want spam accounts to be banned.
If you put the bot in your server with no permission and a spammer joins, it will be banned in any other server it has permission in, so uh, invite it :)

Its only command it \`+stats\`, which shows some statistics for the bot, and a link to invite it.

Enjoy a spam-free Discord experience! <:moonlightwave:493076825417973760>
`).catch(console.error);
  }
});

client.login(config.token);
